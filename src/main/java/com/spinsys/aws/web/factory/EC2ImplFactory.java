package com.spinsys.aws.web.factory;

import com.spinsys.aws.ec2monitor.util.Parameters;
import com.spinsys.aws.ec2monitor.util.UnsupportedActionException;
import com.spinsys.aws.ec2monitor.util.Constants.ActionType;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.aws.web.api.ec2.ListImpl;
import com.spinsys.aws.web.api.ec2.RunstateImpl;

public class EC2ImplFactory
{
	/**
	 * Return an instance of the Amazon Elastic Compute Cloud API
	 * implementation class specified by the given action type.
	 * 
	 * @param actionType  the EC2 instance action to perform.
	 * @param config  the configuration parameters.
	 * @return  the corresponding API implementation class.
	 */
	public static AWSImpl getEC2Implementation( ActionType actionType, Parameters config )
	throws UnsupportedActionException
	{
		AWSImpl impl = null;
		switch (actionType) {
			case DESCRIBE:
				impl = new ListImpl( config );
				break;
			case MONITOR:
				//impl = new MonitorImpl( config );
				break;
			case START:
			case STOP:
				impl = new RunstateImpl( config );
				break;
			default:
				throw new UnsupportedActionException( String.format("Unsupported action type '%s'.", actionType.name()) );
		}

		return impl;
	}
}
