package com.spinsys.aws.web.api.ec2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.AmazonWebServiceResult;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.CapacityReservationSpecificationResponse;
import com.amazonaws.services.ec2.model.CapacityReservationTargetResponse;
import com.amazonaws.services.ec2.model.CpuOptions;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.ElasticGpuAssociation;
import com.amazonaws.services.ec2.model.ElasticInferenceAcceleratorAssociation;
import com.amazonaws.services.ec2.model.EnclaveOptions;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.GroupIdentifier;
import com.amazonaws.services.ec2.model.HibernationOptions;
import com.amazonaws.services.ec2.model.IamInstanceProfile;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceBlockDeviceMapping;
import com.amazonaws.services.ec2.model.InstanceIpv6Address;
import com.amazonaws.services.ec2.model.InstanceMetadataOptionsResponse;
import com.amazonaws.services.ec2.model.InstanceNetworkInterface;
import com.amazonaws.services.ec2.model.InstanceNetworkInterfaceAssociation;
import com.amazonaws.services.ec2.model.InstanceNetworkInterfaceAttachment;
import com.amazonaws.services.ec2.model.InstancePrivateIpAddress;
import com.amazonaws.services.ec2.model.InstanceState;
import com.amazonaws.services.ec2.model.LicenseConfiguration;
import com.amazonaws.services.ec2.model.Monitoring;
import com.amazonaws.services.ec2.model.Placement;
import com.amazonaws.services.ec2.model.ProductCode;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.StateReason;
import com.amazonaws.services.ec2.model.Tag;
import com.spinsys.aws.ec2monitor.util.Parameters;
import com.spinsys.aws.web.api.AWSImpl;
import com.veetechis.lib.text.JSONWriter;
import com.veetechis.lib.util.KeyValuePair;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Simple Storage Service (S3) API implementation to upload files to a storage
 * bucket.
 */
public class ListImpl
extends AWSImpl
{
	/**
	 * Creates an instance of UploadImpl.
	 */
	public ListImpl( Parameters configuration )
	{
		super( configuration );
	}

	
	/**
	 * Retrieves list of EC2 instances and their states.
	 * 
	 * @return  EC2 instances created on the account.
	 * @see  AWSImpl#execute()
	 */
	@Override
	public String execute()
	throws AmazonClientException, AmazonServiceException
	{
		AmazonEC2 client = getClient();

		logInfo( LOG, "Getting started with Amazon EC2 (describe instances)..." );

		DescribeInstancesRequest request = new DescribeInstancesRequest().
			withInstanceIds( getConfiguration().getInstanceIDs() );
		if (getConfiguration().getInstanceIDs().size() < 1) {
			List<Filter> filterList = new ArrayList<Filter>();
			if (getConfiguration().hasResultFilters()) {
				KeyValuePair[] kvps = getConfiguration().getResultFilters().toArray();
				if (kvps != null) {
					for (int i = 0; i < kvps.length; i++) {
						filterList.add( new Filter(kvps[i].getKey(),
							Arrays.asList(kvps[i].getValue().split("\\,"))) );
					}
				}
			}
			request.withMaxResults( getConfiguration().getMaxResults() ).
				withFilters( filterList );
		}

		List<Reservation> reservations = new ArrayList<Reservation>();
		DescribeInstancesResult result = null;
		do {
			result = client.describeInstances( request );
			reservations.addAll( result.getReservations() );
			request.setNextToken( result.getNextToken() );
		}
		while (request.getNextToken() != null);
		result.setReservations( reservations );
		String response = format( result, getConfiguration().getDataFormat() );

		logInfo( LOG, String.format("Received response:\n%s", response) );

		return response;
	}

	/**
	 * Formats and returns the given Simple Storage Service (S3) API call
	 * result in the specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	@Override
	public String format( AmazonWebServiceResult<ResponseMetadata> result, DataFormat format )
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		if (result instanceof DescribeInstancesResult) {
			formatReservations( ((DescribeInstancesResult)result).getReservations(), writer );
		}
		else {
			writer.addQuotedField( "Error", String.format("Unsupported result type - %s", result.getClass().getName()) );
		}
		writer.endDocument();

		return writer.toString();
	}

	protected void formatReservations( List<Reservation> reservations, JSONWriter writer )
	{
		if (reservations != null) {
			writer.startList( "Reservations" );
			for (Reservation reservation: reservations) {
				writer.startObject();
				writer.addStringField( "OwnerId", reservation.getOwnerId() );
				writer.addStringField( "RequesterId", reservation.getRequesterId() );
				writer.addStringField( "ReservationId", reservation.getReservationId() );
				formatGroupNames( reservation.getGroupNames(), writer );
				formatGroups( "Groups", reservation.getGroups(), writer );
				formatInstances( reservation.getInstances(), writer );
				writer.endObject();
			}
			writer.endList();
		}
	}

	protected void formatGroupNames( List<String> names, JSONWriter writer )
	{
		if (names != null) {
			writer.startList( "GroupNames" );
			for (String gn: names) {
				writer.addElement( "\"" + gn + "\"" );
			}
			writer.endList();
		}
	}

	protected void formatGroups( String label, List<GroupIdentifier> groups, JSONWriter writer )
	{
		if (groups != null) {
			writer.startList( label );
			for (GroupIdentifier gid: groups) {
				writer.startObject();
				writer.addStringField( "GroupId", gid.getGroupId() );
				writer.addStringField( "GroupName", gid.getGroupName() );
				writer.endObject();
			}
			writer.endList();
		}
	}

	protected void formatInstances( List<Instance> instances, JSONWriter writer )
	{
		if (instances != null) {
			writer.startList( "Instances" );
			for (Instance inst: instances) {
				writer.startObject();
				writer.addNumberField( "AmiLaunchIndex", inst.getAmiLaunchIndex() );
				writer.addStringField( "Architecture", inst.getArchitecture() );
				writer.addStringField( "BootMode", inst.getBootMode() );
				writer.addStringField( "CapacityReservationId", inst.getCapacityReservationId() );
				writer.addStringField( "ClientToken", inst.getClientToken() );
				writer.addBooleanField( "EbsOptimized", inst.getEbsOptimized() );
				writer.addBooleanField( "EnaSupport", inst.getEnaSupport() );
				writer.addStringField( "Hypervisor", inst.getHypervisor() );
				writer.addStringField( "ImageId", inst.getImageId() );
				writer.addStringField( "InstanceId", inst.getInstanceId() );
				writer.addStringField( "InstanceLifecycle", inst.getInstanceLifecycle() );
				writer.addStringField( "InstanceType", inst.getInstanceType() );
				writer.addStringField( "KernelId", inst.getKernelId() );
				writer.addStringField( "KeyName", inst.getKeyName() );
				writer.addStringField( "LaunchTime", EC2_DATETIME_FORMAT.format(inst.getLaunchTime()) );
				writer.addStringField( "OutpostArn", inst.getOutpostArn() );
				writer.addStringField( "Platform", inst.getPlatform() );
				//writer.addStringField( "PlatformDetails", inst.getPlatformDetails() );
				writer.addStringField( "PrivateDnsName", inst.getPrivateDnsName() );
				writer.addStringField( "PrivateIpAddress", inst.getPrivateIpAddress() );
				writer.addStringField( "PublicDnsName", inst.getPublicDnsName() );
				writer.addStringField( "PublicIpAddress", inst.getPublicIpAddress() );
				writer.addStringField( "RamdiskId", inst.getRamdiskId() );
				writer.addStringField( "RootDeviceName", inst.getRootDeviceName() );
				writer.addStringField( "RootDeviceType", inst.getRootDeviceType() );
				writer.addBooleanField( "SourceDestCheck", inst.getSourceDestCheck() );
				writer.addStringField( "SpotInstanceRequestId", inst.getSpotInstanceRequestId() );
				writer.addStringField( "SriovNetSupport", inst.getSriovNetSupport() );
				writer.addStringField( "StateTransitionReason", inst.getStateTransitionReason() );
				writer.addStringField( "SubnetId", inst.getSubnetId() );
				//writer.addStringField( "UsageOperation", inst.getUsageOperation() );
				//writer.addStringField( "UsageOperationTime", EC2_DATETIME_FORMAT.format(inst.getUsageOperationTime()) );
				writer.addStringField( "VirtualizationType", inst.getVirtualizationType() );
				writer.addStringField( "VpcId", inst.getVpcId() );
				formatBlockDeviceMappings( inst.getBlockDeviceMappings(), writer );
				formatCapacityReservationSpecification( inst.getCapacityReservationSpecification(), writer );
				formatCPUOptions( inst.getCpuOptions(), writer );
				formatElasticGPUAssociations( inst.getElasticGpuAssociations(), writer );
				formatElasticInferenceAcceleratorAssociations( inst.getElasticInferenceAcceleratorAssociations(), writer );
				formatEnclaveOptions( inst.getEnclaveOptions(), writer );
				formatHibernationOptions( inst.getHibernationOptions(), writer );
				formatIAMInstanceProfile( inst.getIamInstanceProfile(), writer );
				formatLicenses( inst.getLicenses(), writer );
				formatMetadataOptions( inst.getMetadataOptions(), writer );
				formatMonitoring( inst.getMonitoring(), writer );
				formatNetworkInterfaces( inst.getNetworkInterfaces(), writer );
				formatPlacement( inst.getPlacement(), writer );
				formatProductCodes( inst.getProductCodes(), writer );
				formatGroups( "SecurityGroups", inst.getSecurityGroups(), writer );
				formatInstanceState( inst.getState(), writer );
				formatStateReason( inst.getStateReason(), writer );
				formatInstanceTags( inst.getTags(), writer );
				writer.endObject();
			}
			writer.endList();
		}
	}

	protected void formatBlockDeviceMappings( List<InstanceBlockDeviceMapping> mappings, JSONWriter writer )
	{
		if (mappings != null) {
			writer.startList( "BlockDeviceMappings" );
			for (InstanceBlockDeviceMapping bdm: mappings) {
				writer.startObject();
				writer.addStringField( "DeviceName", bdm.getDeviceName() );
				writer.startObject( "Ebs" );
				writer.addStringField( "AttachTime", EC2_DATETIME_FORMAT.format(bdm.getEbs().getAttachTime()) );
				writer.addBooleanField( "DeleteOnTermination", bdm.getEbs().getDeleteOnTermination() );
				writer.addStringField( "Status", bdm.getEbs().getStatus() );
				writer.addStringField( "VolumeId", bdm.getEbs().getVolumeId() );
				writer.endObject();
				writer.endObject();
			}
			writer.endList();
		}
	}

	protected void formatCapacityReservationSpecification( CapacityReservationSpecificationResponse specification, JSONWriter writer )
	{
		if (specification != null) {
			writer.startObject( "CapacityReservationSpecification" );
			writer.addStringField( "CapacityReservationPreference", specification.getCapacityReservationPreference() );
			CapacityReservationTargetResponse target = specification.getCapacityReservationTarget();
			if (target != null) {
				writer.startObject( "CapacityReservationTarget" );
				writer.addStringField( "CapacityReservationId", target.getCapacityReservationId() );
				writer.addStringField( "CapacityReservationResourceGroupArn", target.getCapacityReservationResourceGroupArn() );
				writer.endObject();
			}
			writer.endObject();
		}
	}

	protected void formatCPUOptions( CpuOptions options, JSONWriter writer )
	{
		if (options != null) {
			writer.startObject( "CpuOptions" );
			writer.addNumberField( "CoreCount", options.getCoreCount() );
			writer.addNumberField( "ThreadsPerCore", options.getThreadsPerCore() );
			writer.endObject();
		}
	}

	protected void formatElasticGPUAssociations( List<ElasticGpuAssociation> gpus, JSONWriter writer )
	{
		if (gpus != null) {
			writer.startList( "ElasticGpuAssociations" );
			for (ElasticGpuAssociation gpu: gpus) {
				writer.startObject();
				writer.addStringField( "ElasticGpuAssociationId", gpu.getElasticGpuAssociationId() );
				writer.addStringField( "ElasticGpuAssociationState", gpu.getElasticGpuAssociationState() );
				writer.addStringField( "ElasticGpuAssociationTime", gpu.getElasticGpuAssociationTime() );
				writer.addStringField( "ElasticGpuId", gpu.getElasticGpuId() );
				writer.endObject();
			}
			writer.endList();
		}
	}

	protected void formatElasticInferenceAcceleratorAssociations( List<ElasticInferenceAcceleratorAssociation> inferences, JSONWriter writer )
	{
		if (inferences != null) {
			writer.startList( "ElasticInferenceAcceleratorAssociations" );
			for (ElasticInferenceAcceleratorAssociation eiaa: inferences) {
				writer.startObject();
				writer.addStringField( "ElasticInferenceAcceleratorArn", eiaa.getElasticInferenceAcceleratorArn() );
				writer.addStringField( "ElasticInferenceAcceleratorAssociationId", eiaa.getElasticInferenceAcceleratorAssociationId() );
				writer.addStringField( "ElasticInferenceAcceleratorAssociationState", eiaa.getElasticInferenceAcceleratorAssociationState() );
				writer.addStringField( "ElasticInferenceAcceleratorAssociationTime", EC2_DATETIME_FORMAT.format(eiaa.getElasticInferenceAcceleratorAssociationTime()) );
				writer.endObject();
			}
			writer.endList();
		}
	}

	protected void formatEnclaveOptions( EnclaveOptions options, JSONWriter writer )
	{
		if (options != null) {
			writer.startObject( "EnclaveOptions" );
			writer.addBooleanField( "Enabled", options.getEnabled() );
			writer.endObject();
		}
	}

	protected void formatHibernationOptions( HibernationOptions options, JSONWriter writer )
	{
		if (options != null) {
			writer.startObject( "HibernationOptions" );
			writer.addBooleanField( "Configured", options.getConfigured() );
			writer.endObject();
		}
	}

	protected void formatIAMInstanceProfile( IamInstanceProfile profile, JSONWriter writer )
	{
		if (profile != null) {
			writer.startObject( "IamInstanceProfile" );
			writer.addStringField( "Arn", profile.getArn() );
			writer.addStringField( "Id", profile.getId() );
			writer.endObject();
		}
	}

	protected void formatInstanceState( InstanceState state, JSONWriter writer )
	{
		if (state != null) {
			writer.startObject( "State" );
			writer.addNumberField( "Code", state.getCode() );
			writer.addStringField( "Name", state.getName() );
			writer.endObject();
		}
	}

	protected void formatInstanceTags( List<Tag> tags, JSONWriter writer )
	{
		if (tags != null) {
			writer.startList( "Tags" );
			for (Tag it: tags) {
				writer.startObject();
				writer.addStringField( "Key", it.getKey() );
				writer.addStringField( "Value", it.getValue() );
				writer.endObject();
			}
			writer.endList();
		}
	}

	protected void formatLicenses( List<LicenseConfiguration> licenses, JSONWriter writer )
	{
		if (licenses != null) {
			writer.startList( "Licenses" );
			for (LicenseConfiguration config: licenses) {
				writer.addElement( "\"" + config.getLicenseConfigurationArn() + "\"" );
			}
			writer.endList();
		}
	}

	protected void formatMetadataOptions( InstanceMetadataOptionsResponse options, JSONWriter writer )
	{
		if (options != null) {
			writer.startObject( "MetadataOptions" );
			writer.addStringField( "HttpEndpoint", options.getHttpEndpoint() );
			//writer.addStringField( "HttpProtocolIpv6", options.getHttpProtocolIpv6() );
			writer.addNumberField( "HttpPutResponseHopLimit", options.getHttpPutResponseHopLimit() );
			writer.addStringField( "HttpTokens", options.getHttpTokens() );
			writer.addStringField( "State", options.getState() );
			writer.endObject();
		}
	}

	protected void formatMonitoring( Monitoring options, JSONWriter writer )
	{
		if (options != null) {
			writer.startObject( "Monitoring" );
			writer.addStringField( "State", options.getState() );
			writer.endObject();
		}
	}

	protected void formatPlacement( Placement options, JSONWriter writer )
	{
		if (options != null) {
			writer.startObject( "Placement" );
			writer.addStringField( "Affinity", options.getAffinity() );
			writer.addStringField( "AvailabilityZone", options.getAvailabilityZone() );
			writer.addStringField( "GroupName", options.getGroupName() );
			writer.addStringField( "HostId", options.getHostId() );
			writer.addStringField( "HostResourceGroupArn", options.getHostResourceGroupArn() );
			writer.addNumberField( "PartitionNumber", options.getPartitionNumber() );
			writer.addStringField( "SpreadDomain", options.getSpreadDomain() );
			writer.addStringField( "Tenancy", options.getTenancy() );
			writer.endObject();
		}
	}

	protected void formatProductCodes( List<ProductCode> codes, JSONWriter writer )
	{
		if (codes != null) {
			writer.startList( "ProductCodes" );
			for (ProductCode pc: codes) {
				writer.startObject();
				writer.addStringField( "ProductCodeId", pc.getProductCodeId() );
				writer.addStringField( "ProductCodeType", pc.getProductCodeType() );
				writer.endObject();
			}
			writer.endList();
		}
	}

	protected void formatStateReason( StateReason reason, JSONWriter writer )
	{
		if (reason != null) {
			writer.startObject( "StateReason" );
			writer.addStringField( "Code", reason.getCode() );
			writer.addStringField( "Message", reason.getMessage() );
			writer.endObject();
		}
	}

	protected void formatNetworkInterfaces( List<InstanceNetworkInterface> interfaces, JSONWriter writer )
	{
		if (interfaces != null) {
			writer.startList( "NetworkInterfaces" );
			for (InstanceNetworkInterface net: interfaces) {
				writer.startObject();
				writer.addStringField( "Description", net.getDescription() );
				writer.addStringField( "InterfaceType", net.getInterfaceType() );
				//formatIpv4Prefixes( net.getIpv4Prefixes(), writer );
				//formatIpv6Prefixes( net.getIpv6Prefixes(), writer );
				writer.addStringField( "MacAddress", net.getMacAddress() );
				writer.addStringField( "NetworkInterfaceId", net.getNetworkInterfaceId() );
				writer.addStringField( "OwnerId", net.getOwnerId() );
				writer.addStringField( "PrivateDnsName", net.getPrivateDnsName() );
				writer.addStringField( "PrivateIpAddress", net.getPrivateIpAddress() );
				writer.addBooleanField( "SourceDestCheck", net.getSourceDestCheck() );
				writer.addStringField( "Status", net.getStatus() );
				writer.addStringField( "SubnetId", net.getSubnetId() );
				writer.addStringField( "VpcId", net.getVpcId() );
				formatNetworkInterfaceAssociation( net.getAssociation(), writer );
				formatNetworkInterfaceAttachment( net.getAttachment(), writer );
				formatGroups( "Groups", net.getGroups(), writer );
				formatIpv6Addresses( net.getIpv6Addresses(), writer );
				formatPrivateIpAddresses( net.getPrivateIpAddresses(), writer );
				writer.endObject();
			}
			writer.endList();
		}
	}

	protected void formatNetworkInterfaceAssociation( InstanceNetworkInterfaceAssociation association, JSONWriter writer )
	{
		if (association != null) {
			writer.startObject( "Association" );
			writer.addStringField( "CarrierIp", association.getCarrierIp() );
			writer.addStringField( "IpOwnerId", association.getIpOwnerId() );
			writer.addStringField( "PublicDnsName", association.getPublicDnsName() );
			writer.addStringField( "PublicIp", association.getPublicIp() );
			writer.endObject();
		}
	}

	protected void formatNetworkInterfaceAttachment( InstanceNetworkInterfaceAttachment attachment, JSONWriter writer )
	{
		if (attachment != null) {
			writer.startObject( "Attachment" );
			writer.addStringField( "AttachmentId", attachment.getAttachmentId() );
			writer.addStringField( "AttachTime", EC2_DATETIME_FORMAT.format(attachment.getAttachTime()) );
			writer.addBooleanField( "DeleteOnTermination", attachment.getDeleteOnTermination() );
			writer.addNumberField( "DeviceIndex", attachment.getDeviceIndex() );
			writer.addNumberField( "NetworkCardIndex", attachment.getNetworkCardIndex() );
			writer.addStringField( "Status", attachment.getStatus() );
			writer.endObject();
		}
	}

	protected void formatIpv6Addresses( List<InstanceIpv6Address> addresses, JSONWriter writer )
	{
		if (addresses != null) {
			writer.startList( "Ipv6Addresses" );
			for (InstanceIpv6Address ip6: addresses) {
				writer.addElement( "\"" + ip6.getIpv6Address() + "\"" );
			}
			writer.endList();
		}
	}

	protected void formatPrivateIpAddresses( List<InstancePrivateIpAddress> addresses, JSONWriter writer )
	{
		if (addresses != null) {
			writer.startList( "PrivateIpAddresses" );
			for (InstancePrivateIpAddress ip4: addresses) {
				writer.startObject();
				writer.addBooleanField( "Primary", ip4.getPrimary() );
				writer.addStringField( "PrivateDnsName", ip4.getPrivateDnsName() );
				writer.addStringField( "PrivateIpAddress", ip4.getPrivateIpAddress() );
				formatNetworkInterfaceAssociation( ip4.getAssociation(), writer );
				writer.endObject();
			}
			writer.endList();
		}
	}


	private static final Log LOG = LogFactory.getLog( ListImpl.class );
}
