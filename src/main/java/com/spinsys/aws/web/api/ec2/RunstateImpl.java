package com.spinsys.aws.web.api.ec2;

import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.AmazonWebServiceResult;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.InstanceState;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import com.spinsys.aws.ec2monitor.util.Parameters;
import com.spinsys.aws.web.api.AWSImpl;
import com.veetechis.lib.text.JSONWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Elastic Compute Cloud (EC2) API implementation to start and stop EC2
 * instances.
 */
public class RunstateImpl
extends AWSImpl
{
	/**
	 * Creates an instance of RunstateImpl.
	 */
	public RunstateImpl( Parameters configuration )
	{
		super( configuration );
	}

	
	/**
	 * Starts or stops a set of EC2 instances.
	 * 
	 * @return  EC2 instances started or stopped and their states.
	 * @see  AWSImpl#execute()
	 */
	@Override
	public String execute()
	throws AmazonClientException, AmazonServiceException
	{
		AmazonEC2 client = getClient();

		logInfo( LOG, "Getting started with Amazon EC2 (start|stop instances)..." );

		AmazonWebServiceResult<ResponseMetadata> result;
		List<String> instanceIds = getConfiguration().getInstanceIDs();
		switch (getConfiguration().getActionType()) {
			case START:
				result = client.startInstances( new StartInstancesRequest(instanceIds) );
				break;
			case STOP:
				result = client.stopInstances( new StopInstancesRequest(instanceIds) );
				break;
			default:
				throw new AmazonClientException( String.format("Unsupported runstate action type '%s'.", getConfiguration().getActionType().toString()) );
		}
		String response = format( result, getConfiguration().getDataFormat() );

		logInfo( LOG, String.format("Received response:\n%s", response) );

		return response;
	}

	/**
	 * Formats and returns the given Simple Storage Service (S3) API call
	 * result in the specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	@Override
	public String format( AmazonWebServiceResult<ResponseMetadata> result, DataFormat format )
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		if (result instanceof StartInstancesResult) {
			formatRunstateResult( "StartingInstances", ((StartInstancesResult)result).getStartingInstances(), writer );
		}
		else if (result instanceof StopInstancesResult) {
			formatRunstateResult( "StoppingInstances", ((StopInstancesResult)result).getStoppingInstances(), writer );
		}
		else {
			writer.addQuotedField( "Error", String.format("Unsupported result type - %s", result.getClass().getName()) );
		}
		writer.endDocument();

		return writer.toString();
	}

	protected void formatRunstateResult( String action, List<InstanceStateChange> stateChangeList, JSONWriter writer )
	{
		if (stateChangeList != null) {
			writer.startList( action );
			for (InstanceStateChange stateChange: stateChangeList) {
				writer.startObject();
				writer.addStringField( "InstanceId", stateChange.getInstanceId() );
				formatRunstate( "CurrentState", stateChange.getCurrentState(), writer );
				formatRunstate( "PreviousState", stateChange.getPreviousState(), writer );
				writer.endObject();
			}
			writer.endList();
		}
	}

	protected void formatRunstate( String runstate, InstanceState state, JSONWriter writer )
	{
		if (state != null) {
			writer.startObject( runstate );
			writer.addNumberField( "Code", state.getCode() );
			writer.addStringField( "Name", state.getName() );
			writer.endObject();
		}
	}


	private static final Log LOG = LogFactory.getLog( RunstateImpl.class );
}
