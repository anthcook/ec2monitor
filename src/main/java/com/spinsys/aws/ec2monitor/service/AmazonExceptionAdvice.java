package com.spinsys.aws.ec2monitor.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.veetechis.lib.text.JSONWriter;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class AmazonExceptionAdvice
{
	@ResponseBody
	@ExceptionHandler( AmazonClientException.class )
	@ResponseStatus( HttpStatus.BAD_GATEWAY )
	String amazonClientExceptionHandler( AmazonClientException e )
	{
		return makeErrorResponse( e );
	}

	@ResponseBody
	@ExceptionHandler( AmazonServiceException.class )
	@ResponseStatus( HttpStatus.BAD_REQUEST )
	String amazonServiceExceptionHandler( AmazonServiceException e )
	{
		return makeErrorResponse( e );
	}

	private String makeErrorResponse( AmazonClientException e )
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		writer.addStringField( "Exception", e.getClass().getName() );
		writer.addStringField( "Message", e.getMessage() );
		if (e instanceof AmazonServiceException) {
			formatServiceException( (AmazonServiceException)e, writer );
		}
		writer.endDocument();

		return writer.toString();
	}

	private void formatServiceException( AmazonServiceException e, JSONWriter writer )
	{
		writer.addStringField( "ProxyHost", e.getProxyHost() );
		writer.addStringField( "RequestId", e.getRequestId() );
		writer.addStringField( "ServiceName", e.getServiceName() );

		writer.startObject( "AwsError" );
		writer.addStringField( "ErrorCode", e.getErrorCode() );
		writer.addStringField( "ErrorMessage", e.getErrorMessage() );
		writer.addStringField( "ErrorType", e.getErrorType().toString() );
		writer.endObject();

		writer.startObject( "HttpHeaders" );
		for (String header: e.getHttpHeaders().keySet()) {
			writer.addStringField( header, e.getHttpHeaders().get(header) );
		}
		writer.endObject();
	}
}
