/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spinsys.aws.ec2monitor.service;

import com.spinsys.aws.ec2monitor.util.Constants;

/**
 *
 * @author acook@spinsys.com
 */
public class EC2Monitor
implements Constants
{

	public EC2Monitor( ActionType action, DataFormat format, String response )
	{
		this.action = action;
		this.format = format;
		this.response = response;
	}
	
	public ActionType getActionType()
	{
		return action;
	}
	
	public DataFormat getDataFormat()
	{
		return format;
	}
	
	public String getResponse()
	{
		return response;
	}

	
	private final ActionType action;
	private final DataFormat format;
	private final String response;
}
