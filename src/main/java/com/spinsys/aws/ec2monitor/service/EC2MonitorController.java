package com.spinsys.aws.ec2monitor.service;

import java.util.Hashtable;
import java.util.Properties;

import com.spinsys.aws.ec2monitor.service.EC2MonitorController;
import com.spinsys.aws.ec2monitor.util.Constants;
import com.spinsys.aws.ec2monitor.util.Parameters;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.aws.web.factory.EC2ImplFactory;
import com.veetechis.lib.text.JSONWriter;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * REST controller for EC2 action requests.
 * 
 * @author acook@spinsys.com
 */
@RestController
public class EC2MonitorController
implements Constants
{

	@PostMapping("/login")
	public String login(
		@RequestParam(value = "clientid") String clientId,
		@RequestParam(value = "accesskey") String accessKey,
		@RequestParam(value = "secretkey") String secretKey,
		@RequestParam(value = "region", defaultValue = "us-east-1") String region,
		@RequestParam(value = "sessiontoken", defaultValue = "") String sessionToken )
	{
		Properties configProps = new Properties();
		configProps.setProperty( Parameter.AWS_REGION.property, region );
		configProps.setProperty( Parameter.AWS_ACCESS_KEY.property, accessKey );
		configProps.setProperty( Parameter.AWS_SECRET_KEY.property, secretKey );
		if (sessionToken.length() > 0) {
			configProps.setProperty( Parameter.AWS_SESSION_TOKEN.property, sessionToken );
		}
		configProps.setProperty( Parameter.ACTION_TYPE.property, ActionType.DESCRIBE.name() );

		Parameters config = new Parameters( configProps );

		String response = performAction( config );
		sessions.put( clientId, config );

		return response;
	}

	@PostMapping("/runstate")
	public String changeState(
		@RequestParam(value = "clientid") String clientId,
		@RequestParam(value = "instanceid") String instanceId,
		@RequestParam(value = "state") String state )
	{
		Parameters config = sessions.get( clientId );
		config.setActionType( ActionType.valueOf(state.toUpperCase()) );
		config.getInstanceIDs().clear();
		config.getInstanceIDs().add( instanceId );

		return performAction( config );		
	}

	@GetMapping("/info")
	public String instanceInfo(
		@RequestParam(value = "clientid") String clientId,
		@RequestParam(value = "instanceid") String instanceId )
	{
		Parameters config = sessions.get( clientId );
		config.setActionType( ActionType.DESCRIBE );
		config.getInstanceIDs().clear();
		config.getInstanceIDs().add( instanceId );

		return performAction( config );		
	}

	@GetMapping("/session")
	public String sessionInfo( @RequestParam(value = "clientid") String clientId )
	{
		Parameters config = sessions.get( clientId );

		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		writer.addStringField( "clientId", clientId );
		if (config != null) {
			writer.addStringField( "config", config.toString() );
		}
		else {
			writer.addStringField( "config", "NotFound" );
		}
		writer.endDocument();

		return writer.toString();
	}

	
	protected String performAction( Parameters config )
	{
		AWSImpl ec2 = EC2ImplFactory.getEC2Implementation( config.getActionType(), config );
		return ec2.execute();
	}

	
	private final Hashtable<String, Parameters> sessions = new Hashtable<String, Parameters>();
}
