package com.spinsys.aws.ec2monitor;

import java.io.File;
import java.io.FileOutputStream;

import com.spinsys.aws.ec2monitor.util.Constants;
import com.spinsys.aws.ec2monitor.util.Parameters;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.aws.web.factory.EC2ImplFactory;
import com.spinsys.util.Utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EC2MonitorApplication
{
	public static void main( String[] args )
	{
		if (args.length > 0) {
			try {
				Parameters config = new Parameters( args );
				if (!config.hasCredentials() || !config.hasRequiredParameters()) {
					System.out.println( config.getUsage() );
				}
				else {
					AWSImpl ec2 = EC2ImplFactory.getEC2Implementation( config.getActionType(), config );
					String result = ec2.execute();
					if (config.getResponseFile() != null) {
						writeResponse( result, config );
					}
				}
			}
			catch (Exception e) {
				if (LOG.isErrorEnabled()) {
					LOG.error( "Error running client.", e );
				}
			}
		}
		else {
			SpringApplication.run( EC2MonitorApplication.class, args );
		}
	}


	/*
	 * Writes the give response to one of the following:
	 * 1. The output file path specified in parameters if not a directory; or
	 *
	 * 2. The output directory specified in parameters with file name same as
	 * the input file but with '.rsp' suffix (e.g. --file=myinput.json ->
	 * myinput.rsp); or
	 *
	 * 3. The output directory specified in parameters with file name same as
	 * the specified document type with suffix '.rsp' if input file not
	 * specified.
	 */
	private static File writeResponse( String response, Parameters config )
	{
		File responseFile = config.getResponseFile();
		Constants.ActionType actionType = config.getActionType();

		File outFile;
		if (responseFile.isDirectory()) {
			outFile = new File( responseFile, String.format("%s.rpt", actionType.toString()) );
		}
		else {
			outFile = responseFile;
		}
		
		try (FileOutputStream out = new FileOutputStream(outFile)) {
			out.write( Utils.beautify(response).getBytes() );
			out.flush();
			System.out.println( String.format("%s report written to file %s.", actionType.toString(), outFile.toString()) );
		}
		catch( Exception e ) {
			outFile = null;
			if (LOG.isErrorEnabled()) {
				LOG.error( String.format("Error creating response file %s.", outFile) );
			}
		}
		
		return outFile;
	}

	
	/*
	static {
		String path = CostToolMain.class.getClassLoader().
				getResource( "logging.properties" ).getFile();
		System.setProperty( "java.util.logging.config.file", path );
		System.setProperty( "org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger" );
	}
	*/
	
	private static final Log LOG = LogFactory.getLog( EC2MonitorApplication.class );
}
