package com.spinsys.aws.ec2monitor.util;

import java.text.SimpleDateFormat;

public interface Constants
{
	public static enum DataFormat {
		JSON( "application/json" ),
		TEXT( "text/plain" ),
		TABLE( "text/csv" );
		
		public String getMimeType() { return mimeType; }
		
		private DataFormat( String mimeType ) {
			this.mimeType = mimeType;
		}
		
		private final String mimeType;
	}
	
	public static enum ActionType {
		DESCRIBE(), MONITOR(), START(), STOP();
		
		public String toString()
		{
			return normalize( name() );
		}

		private String normalize( String word )
		{
			return new StringBuilder( word.toUpperCase().substring(0, 1) ).
				append( word.toLowerCase().substring(1) ).toString();
		}

		private ActionType() {}
	}

	public static enum Parameter {
		CONFIG_FILE( "config", "--config" ),
		AWS_ACCOUNT_ID( "accountid", "--accountid" ),
		AWS_PROFILE( "profile", "--profile" ),
		AWS_ACCESS_KEY( "accesskey", "--accesskey" ),
		AWS_SECRET_KEY( "secretkey", "--secretkey" ),
		AWS_SESSION_TOKEN( "sessiontoken", "--sessiontoken" ),
		AWS_REGION( "region", "--region" ),
		OUTPUT_PATH( "output", "--output" ),
		DATA_FORMAT( "format", "--format" ),
		ACTION_TYPE( "action", "--action" ),
		INSTANCE_IDS( "instanceids", "--instanceids" ),
		FILTER_BY( INSTANCE_FILTER, "--filterby" ),
		MAX_RESULTS( "maxresults", "--maxresults" );

		private Parameter( String property, String argument )
		{
			this.property = property;
			this.argument = argument;
		}

		public String toString()
		{
			return new StringBuilder( name() ).append( "{" ).
				append( "property: " ).append( property ).append( ", " ).
				append( "argument: " ).append( argument ).append( "}" ).
				toString();
		}

		public final String property, argument;
	}

	public final static Integer RESULTS_MAXIMUM = Integer.valueOf( 100 );
	public final static String AWS_ACCESS_KEY_ID_PROPERTY = "aws.accessKeyId";
	public final static String AWS_SECRET_ACCESS_KEY_PROPERTY = "aws.secretKey";

	public final static String INSTANCE_FILTER = "instance.filter";
	public final static String FILTER_BY_FIELD_TMPL = INSTANCE_FILTER + ".%s.field.%d";
	public final static String FILTER_BY_VALUE_TMPL = INSTANCE_FILTER + ".%s.value.%d";

	public final static SimpleDateFormat EC2_DATETIME_FORMAT = new SimpleDateFormat( "EEE MMM dd HH:mm:ss zzz yyyy" );
}
