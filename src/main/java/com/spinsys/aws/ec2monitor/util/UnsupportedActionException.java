package com.spinsys.aws.ec2monitor.util;


/**
 * Thrown to indicate an unknown or unsupported EC2 action is requested.
 * 
 * @author acook@spinsys.com
 */
public class UnsupportedActionException
extends UnsupportedOperationException
{
	public UnsupportedActionException( String message )
	{
		super( message );
	}
	
	public UnsupportedActionException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
