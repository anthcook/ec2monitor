package com.spinsys.aws.ec2monitor.util;

import com.veetechis.lib.io.FileUtils;
import com.veetechis.lib.preferences.ArgumentsParser;
import com.veetechis.lib.util.KeyValueException;
import com.veetechis.lib.util.KeyValueList;
import com.veetechis.lib.util.KeyValuePair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class Parameters
implements Constants
{
	/**
	 * Default configuration value indicates a value must be provided by the
	 * user.
	 */
	protected static final String USER_MUST_PROVIDE = "userMustProvide";

	/**
	 * Creates a new instance with the given arguments.
	 * 
	 * @param args  the arguments to parse.
	 * @throws Exception  if a parsing error occurs.
	 */
	public Parameters( String[] args )
	throws Exception
	{
		if (args.length > 0) {
			String outputPath = null;
			KeyValueList parms = new ArgumentsParser( ArgumentsParser.DEFAULT_SWITCH, "=" ).parse( args );
			if (parms.get(Parameter.CONFIG_FILE.argument) != null) {
				readConfig( parms.get(Parameter.CONFIG_FILE.argument).getValue() );
				outputPath = config.getProperty( "outputPath" );
			}
			if (parms.get(Parameter.AWS_ACCOUNT_ID.argument) != null) {
				accountId = parms.get( Parameter.AWS_ACCOUNT_ID.argument ).getValue();
			}
			if (parms.get(Parameter.AWS_PROFILE.argument) != null) {
				profile = parms.get( Parameter.AWS_PROFILE.argument ).getValue();
			}
			if (parms.get(Parameter.AWS_ACCESS_KEY.argument) != null) {
				accessKey = parms.get( Parameter.AWS_ACCESS_KEY.argument ).getValue();
			}
			if (parms.get(Parameter.AWS_SECRET_KEY.argument) != null) {
				secretKey = parms.get( Parameter.AWS_SECRET_KEY.argument ).getValue();
			}
			if (parms.get(Parameter.AWS_SESSION_TOKEN.argument) != null) {
				sessionToken = parms.get( Parameter.AWS_SESSION_TOKEN.argument ).getValue();
			}
			if (parms.get(Parameter.AWS_REGION.argument) != null) {
				region = parms.get( Parameter.AWS_REGION.argument ).getValue().toLowerCase();
			}
			if (parms.get(Parameter.OUTPUT_PATH.argument) != null) {
				outputPath = parms.get( Parameter.OUTPUT_PATH.argument ).getValue();
			}
			if (parms.get(Parameter.MAX_RESULTS.argument) != null) {
				setMaxResults( parms.get(Parameter.MAX_RESULTS.argument).getValue() );
			}
			if (parms.get(Parameter.DATA_FORMAT.argument) != null) {
				dataFormat = DataFormat.valueOf( parms.get(Parameter.DATA_FORMAT.argument).getValue().toUpperCase() );
			}
			if (parms.get(Parameter.ACTION_TYPE.argument) != null) {
				setActionType(
					parms.get(Parameter.ACTION_TYPE.argument).getValue(),
					parms.get(Parameter.INSTANCE_IDS.argument) );
			}

			if (parms.get(Parameter.FILTER_BY.argument) != null) {
				config.setProperty( Parameter.FILTER_BY.property, parms.get(Parameter.FILTER_BY.argument).getValue() );
			}
			
			if (outputPath != null) {
				responseFile = new File( outputPath );
			}
		}
	}

	/**
	 * Creates a new instance with the given configuration properties.
	 * 
	 * @param config  the properties to parse.
	 * @throws Exception  if a parsing error occurs.
	 */
	public Parameters( Properties config )
	{
		this.config = config;
		validateConfig();
		
		String outputPath = config.getProperty( "outputPath" );
		if (outputPath != null) {
			responseFile = new File( outputPath );
		}
	}

	public String getAccountID()
	{
		return accountId;
	}

	public String getAccessKey()
	{
		return accessKey;
	}

	public String getSecretKey()
	{
		return secretKey;
	}

	public String getSessionToken()
	{
		return sessionToken;
	}

	public String getSecurityProfile()
	{
		return profile;
	}

	public String getRegion()
	{
		return region;
	}

	public File getResponseFile()
	{
		return responseFile;
	}

	public void setResponseFile( File rfile )
	{
		responseFile = rfile;
	}

	public DataFormat getDataFormat()
	{
		return dataFormat;
	}

	public ActionType getActionType()
	{
		return actionType;
	}

	public void setActionType( ActionType actionType )
	{
		this.actionType = actionType;
	}

	public void addInstanceIDs( List<String> instanceIds )
	{
		if (instanceIds != null) {
			this.instanceIds.addAll( instanceIds );
		}
	}

	public List<String> getInstanceIDs()
	{
		return instanceIds;
	}

	public boolean hasResultFilters()
	{
		return (validate(config.getProperty(Parameter.FILTER_BY.property)) != null);
	}

	public KeyValueList getResultFilters()
	{
		KeyValueList filters = new KeyValueList();

		String filterSet = config.getProperty( Parameter.FILTER_BY.property );
		String type, value;
		for (int i = 1; i < Integer.MAX_VALUE; i++) {
			type = config.getProperty( String.format(FILTER_BY_FIELD_TMPL, filterSet, i) );
			value  = config.getProperty( String.format(FILTER_BY_VALUE_TMPL, filterSet, i) );
			if (validate(type) != null && validate(value) != null) {
				try {
					filters.add( new KeyValuePair(type, value) );
				}
				catch (KeyValueException e) {
					if (LOG.isWarnEnabled()) {
						LOG.warn( String.format(
							"Unexpected error while parsing price filter '%s'.", filterSet), e );
					}
				}
			}
			else {
				break;
			}
		}

		return filters;
	}

	public Integer getMaxResults()
	{
		if ((maxResults != null) && (maxResults < 1 || maxResults > 100)) {
			if (LOG.isWarnEnabled()) {
				LOG.warn( String.format("Unsupported max results value '%s'; default is being used.", maxResults) );
			}
			maxResults = null;
		}

		return (maxResults != null ? maxResults : RESULTS_MAXIMUM);
	}

	public boolean hasCredentials()
	{
		boolean hasCreds = hasConfigCredentials();
		if (!hasCreds) {
			hasCreds = (new File(System.getProperty("user.home"), ".aws/credentials")).exists();
		}

		return hasCreds;
	}
	
	public boolean hasSessionToken()
	{
		return (sessionToken != null);
	}
	
	public boolean hasConfigCredentials()
	{
		return (accessKey != null && secretKey != null);
	}
	
	public boolean hasRequiredParameters()
	{
		return (dataFormat != null && actionType != null);
	}

	public String getUsage()
	{
		return "Usage parameters:\n" +
				"\t--config=<configuration properties file>\n" +
				"\t--profile=<AWS security profile>\n" +
				"\t--accountid=<account ID to access>\n" +
				"\t--accesskey=<account access key ID>\n" +
				"\t--secretkey=<account secret access key>\n" +
				"\t--sessiontoken=<temporary session token>\n" +
				"\t--region=<account region [e.g. us-east-1]>\n" +
				"\t--output=<path to write response>\n" +
				"\t--format=<response format type [e.g. json|table|text]>\n" +
				"\t--action=<the instance action type [1. Describe|2. Monitor|3. Start|4. Stop]>\n" +
				"\t--instanceids=<the instance ID(s)>\n" +
				"\t--filterby=<user defined instance filter [see README]>\n" +
				"\t--maxresults=<the maximum number of results to return (1-100)>\n\n" +
				"** An output path can be a named file or a directory. If a directory then\n" +
				"   the file name used is the action type_instanceid with suffix matching the\n" +
				"   data format [e.g. DescribeInstances_id-1234.json]. If an output path isn't\n" +
				"   specified then the action result is displayed on the console.\n\n" +
				"   The supported action types include:\n" +
				"\t1. Describe EC2 Instances - list of EC2 instances created for the account\n" +
				"\t       with their current status and machine details.\n" +
				"\t2. Monitor EC2 Instances - current EC2 instance status and run metrics.\n" +
				"\t3. Start EC2 Instances - start one or more currently stopped EC2 instances.\n" +
				"\t3. Stop EC2 Instances - stop one or more currently running EC2 instances.\n";
	}
	
	public String toString()
	{
		StringBuilder out = new StringBuilder( "[" ).
			append( "Account ID = " ).append( getAccountID() ).append( "; " ).
			append( "Access Key = " ).append( getAccessKey() ).append( "; " ).
			append( "Secret Key = " ).append( getSecretKey() ).append( "; " ).
			append( "AWS Region = " ).append( getRegion() ).append( "; " ).
			append( "Action Type = " ).append( getActionType() ).append( "; " ).
			append( "Instance IDs = " ).append( getInstanceIDs() ).append( "; " ).
			append( "Response Format = " ).append( getDataFormat() ).append( "; " ).
			append( "Output Path = " ).append( getResponseFile() ).append( "; " ).
			append( "Max Results = " ).append( getMaxResults() ).
			append( "]" );

		return out.toString();
	}


	/**
	 * Validates the given argument value. Returns the value if validated,
	 * otherwise returns null.
	 * 
	 * @param argValue  the value to validate.
	 * @return  the validated value or null.
	 */
	protected String validate( String argValue )
	{
		String rval = argValue;

		if (USER_MUST_PROVIDE.equalsIgnoreCase(argValue)) {
			rval = null;
		}

		return rval;
	}


	private void setActionType( String type, KeyValuePair instanceIds )
	{
		List<String> idsList = new ArrayList<String>();
		if (instanceIds != null) {
			String[] ids = instanceIds.getValue().split( "\\," );
			idsList.addAll( Arrays.asList(ids) );
		}
		setActionType( type, idsList );
	}

	private void setActionType( String type, List<String> instanceIds )
	{
		actionType = ActionType.valueOf( type.toUpperCase() );
		addInstanceIDs( instanceIds );
	}

	private void setMaxResults( String maximum )
	{
		if (validate(maximum) != null) {
			try {
				maxResults = Integer.valueOf( maximum );
			}
			catch (NumberFormatException e) {
				if (LOG.isErrorEnabled()) {
					LOG.error( String.format("Invalid value for property '%s': %s",
						Parameter.MAX_RESULTS.property, maximum) );
				}
			}
		}
	}

	private void readConfig( String configPath )
	throws IOException
	{
		File pfile = new File( configPath );
		config = new Properties();
		try {
			if (FileUtils.isReadableFile(pfile)) {
				config.load( new FileInputStream(pfile) );
				validateConfig();
			}
			else {
				if (LOG.isWarnEnabled()) {
					LOG.warn( String.format("Parameters file '%s' not readable, input ignored.", pfile) );
				}
			}
		}
		catch (FileNotFoundException e) {
			if (LOG.isWarnEnabled()) {
				LOG.warn( String.format("Parameters file '%s' not found, input ignored.", pfile) );
			}
		}
	}

	private void validateConfig()
	{
		accountId = validate( config.getProperty(Parameter.AWS_ACCOUNT_ID.property) );
		accessKey = validate( config.getProperty(Parameter.AWS_ACCESS_KEY.property) );
		secretKey = validate( config.getProperty(Parameter.AWS_SECRET_KEY.property) );
		sessionToken = validate( config.getProperty(Parameter.AWS_SESSION_TOKEN.property) );
		profile = validate( config.getProperty(Parameter.AWS_PROFILE.property) );
		region = validate( config.getProperty(Parameter.AWS_REGION.property) );
		if (validate(config.getProperty(Parameter.DATA_FORMAT.property)) != null) {
			dataFormat = DataFormat.valueOf(
				config.getProperty(Parameter.DATA_FORMAT.property).toUpperCase() );
		}
		if (validate(config.getProperty(Parameter.ACTION_TYPE.property)) != null) {
			actionType = ActionType.valueOf(
				config.getProperty(Parameter.ACTION_TYPE.property).toUpperCase() );
			if (validate(config.getProperty(Parameter.INSTANCE_IDS.property)) != null) {
				String[] ids = config.getProperty( Parameter.INSTANCE_IDS.property ).split( "\\," );
				addInstanceIDs( Arrays.asList(ids) );
			}
		}
		setMaxResults( config.getProperty(Parameter.MAX_RESULTS.property) );
		String outputPath = validate( config.getProperty(Parameter.OUTPUT_PATH.property) );
		if (outputPath != null) {
			config.setProperty( "outputPath", outputPath );
		}
	}


	private Integer maxResults;
	private String accountId;
	private String profile;
	private String accessKey;
	private String secretKey;
	private String sessionToken;
	private String region;
	private File responseFile;
	private Properties config;
	private DataFormat dataFormat;
	private ActionType actionType;

	private List<String> instanceIds = new ArrayList<String>();

	private static final Log LOG = LogFactory.getLog( Parameters.class );
}
