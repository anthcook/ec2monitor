'use strict';

angular.module('monitor', [
	'ngRoute',
	'ec2monitorApp',
	'authorize'
]);
angular.
	module('monitor').
	component('monitor', {
		templateUrl: 'monitor/monitor.html',
		controller: ['appService',  function MonitorController(appService) {
			var self = this;

			// reservations: [{
			//   id:
			//   ownerId:
			//   instances: [{
			//     name:
			//     id:
			//     vpcId:
			//     type:
			//     state:
			//     action:
			//     publicIp:
			//     publicDns:
			//   }]
			// ]}
			self.reservations = [];

			self.performAction = function(reservationId, instanceId) {
				instance:
				for (var i = 0; i < self.reservations.length; i++) {
					if (self.reservations[i].id === reservationId) {
						var reservation = self.reservations[i];
						for (var j = 0; j < reservation.instances.length; j++) {
							if (reservation.instances[j].id === instanceId) {
								var action = reservation.instances[j].action;
								if (action.toUpperCase() === 'UPDATE') {
									appService.update(instanceId, onUpdateSuccess, onError);
								}
								else {
									appService.runstate(instanceId, action, onActionSuccess, onError);
								}
								break instance;
							}
						}
					}
				}
			}

			var onActionSuccess = function(response) {
				var update = response.data;
				var ilist = [];
				if (update.StartingInstances !== undefined) {
					ilist = update.StartingInstances;
				}
				else if (update.StoppingInstances !== undefined) {
					ilist = update.StoppingInstances
				}
				else {
					ilist[0] = {
						'InstanceId': 'Error',
						'PreviousState': { 'Name': 'Error' },
						'CurrentState': { 'Name': 'Error' }
					}
				}

				var instanceId = ilist[0].InstanceId;
				var currState = ilist[0].CurrentState.Name;
				if (instanceId !== 'Error') {
					updateState:
					for (var i = 0; i < self.reservations.length; i++) {
						var reservation = self.reservations[i];
						for (var j = 0; j < reservation.instances.length; j++) {
							var instance = reservation.instances[j];
							if (instance.id === instanceId) {
								instance.state = currState.toUpperCase();
								instance.action = 'Update';
								instance.actionStyle = 'updateButton';
								break updateState;
							}
						}
					}
				}
				else {
					console.log( 'Unexpected response received: ' + JSON.stringify(update) );
					alert( 'Alert something went wrong. See console log.' );
				}
			}

			var onUpdateSuccess = function(response) {
				var data = response.data;
				for (var i = 0; i < data.Reservations.length; i++) {
					updateInstances(data.Reservations[i]);
				}
			}

			var onError = function(response) {
				alert( 'Error status received: ' + response.status );
			};

			var updateInstances = function(data) {
				var reservation = {
					'id': data.ReservationId,
					'ownerId': data.OwnerId,
					'instances': []
				};

				for (var i = 0; i < data.Instances.length; i++) {
					var rri = data.Instances[i];
					var instance = {
						'id': rri.InstanceId,
						'vpcId': rri.VpcId,
						'type': rri.InstanceType,
						'state': rri.State.Name.toUpperCase(),
						'publicIp': '',
						'publicDns': '',
						'name': '',
						'action': '',
						'actionStyle': ''
					};
					for (var j = 0; j < rri.Tags.length; j++) {
						if (rri.Tags[j].Key === 'Name') {
							instance.name = rri.Tags[j].Value;
							break;
						}
					}
					if (instance.state === 'STOPPED') {
						instance.action = 'Start';
						instance.actionStyle = 'startButton';
						instance.publicIp = 'Not Assigned';
						instance.publicDns = 'Not Assigned';
					}
					else if (instance.state === 'RUNNING') {
						instance.action = 'Stop';
						instance.actionStyle = 'stopButton';
						instance.publicIp = rri.PublicIpAddress;
						instance.publicDns = rri.PublicDnsName;
					}
					else {
						instance.action = 'Update';
						instance.actionStyle = 'updateButton';
						instance.publicIp = 'Pending';
						instance.publicDns = 'Pending';
					}
					reservation.instances[i] = instance;
				}

				updateReservations(reservation);
			}

			var updateReservations = function(data) {
				var updated = false;
				for (var i = 0; i < self.reservations.length; i++) {
					if (self.reservations[i].id === data.id) {
						self.reservations[i] = data;
						updated = true;
						break;
					}
				}
				if (!updated) {
					var index = self.reservations.length;
					self.reservations[index] = data;
				}
			}

			var init = function() {
				var response = appService.getReservations();
				for (var i = 0; i < response.Reservations.length; i++) {
					updateInstances(response.Reservations[i]);
				}
			}

			init();
		}]
	});
